import React, { Component } from "react";
import axios from "axios";
import Header from "./layout/Header";
import Main from "./layout/Main";
import Footer from "./layout/Footer";
import Overlay from "./layout/Overlay";
import Error from "./layout/Error";
import SetupRequest from "./control/SetupRequest";

export default class App extends Component {
  state = {
    isLoading: false,
    isError: false,
    error: null,
    data: null,
    getError: false,
    delay: 1,
    source: null
  };

  getData = () => {
    const { delay, getError } = this.state;
    const source = axios.CancelToken.source();

    this.setState({ isLoading: true, isError: false, source });

    axios
      .get(`/api/${delay}/${getError}`, { cancelToken: source.token })
      .then(({ data }) => {
        this.setState({ data: data, isLoading: false });
      })
      .catch(error => {
        const msg =
          error.response && error.response.data
            ? error.response.data
            : error.message
            ? error.message
            : "Error";

        this.setState({
          isError: true,
          error: msg,
          isLoading: false
        });
      });
  };

  hideError = () => {
    this.setState({ isError: false, error: null });
  };

  changeVal = e => {
    const val = e.target.name === "delay" ? e.target.value : e.target.checked;
    this.setState({ [e.target.name]: val });
  };

  cancel = () => {
    const { source } = this.state;
    source.cancel("Operation canceled by the user.");
  };

  render() {
    const { isLoading, data, isError, error, delay, getError } = this.state;

    return (
      <div className="app">
        {/* Header */}
        <Header />

        {/* Main */}
        <main className="main">
          <div className="container">
            <h1>Hook Up and Make a Request</h1>
            <SetupRequest
              getError={getError}
              delay={delay}
              action={this.changeVal}
            />
            <Main action={this.getData} data={data} />
          </div>
        </main>

        {/* Footer */}
        <Footer />

        {/* Error */}
        <Error isError={isError} error={error} action={this.hideError} />

        {/* Overlay */}
        <Overlay isLoading={isLoading} action={this.cancel} />
      </div>
    );
  }
}
