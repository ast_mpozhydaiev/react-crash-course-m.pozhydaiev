import React from "react";

export default function SetupRequest({ delay, getError, action }) {
  return (
    <div className="block">
      <h2>Settings:</h2>
      <div className="settings">
        <div className="block settings__element">
          <h3>Delay (s)</h3>
          <input
            type="range"
            name="delay"
            value={delay}
            min="1"
            max="5"
            onChange={action}
          />
          <p>{delay} sec</p>
        </div>

        <div  className="block settings__element">
          <h3>
            <label>
              Should Get an Error -
              <input
                type="checkbox"
                name="getError"
                checked={getError}
                onChange={action}
              />
            </label>
          </h3>
        </div>
      </div>
    </div>
  );
}
