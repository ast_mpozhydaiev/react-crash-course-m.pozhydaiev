import React from "react";

export default function Error({ isError, error, action }) {
  return (
    <>
      {isError && (
        <div className="error">
          <div className="error__content">
            <p>{error}</p>
            <button className="btn btn_error" onClick={action}>Ok</button>
          </div>
        </div>
      )}
    </>
  );
}
