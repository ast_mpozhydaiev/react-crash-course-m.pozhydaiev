import React from "react";
import Profile from "../content/Profile";

export default function Main({ action, data, isLoading, isError }) {
  return (
    <div className="block">
      <h2>Launch</h2>
      {data && (
        <ul>
          {data.map(profile => (
            <Profile key={profile.id} profile={profile} />
          ))}
        </ul>
      )}
      <button className="btn" onClick={action}>launch</button>
    </div>
  );
}
