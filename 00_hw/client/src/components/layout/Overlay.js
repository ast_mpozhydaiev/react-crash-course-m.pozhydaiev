import React from "react";
import spinner from "../../assets/spinner.gif";

export default function Overlay({ isLoading, action }) {
  return (
    <>
      {isLoading && (
        <div className="overlay">
          <div className="overlay__content">
            <img src={spinner} alt="Loading..." className="overlay__img" />
            <button className="btn" type="button" onClick={action}>
              Cancel
            </button>
          </div>
        </div>
      )}
    </>
  );
}
