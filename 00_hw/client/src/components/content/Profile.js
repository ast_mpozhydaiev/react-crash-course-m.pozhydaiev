import React from 'react'

export default function Profile({profile}) {
    return (
        <li className="block">
            <p>{profile.name}</p>
        </li>
    )
}
