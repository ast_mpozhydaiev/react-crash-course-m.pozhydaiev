const { Router } = require("express");
const router = Router();

router.get("/:delay/:shouldGetError", (req, res) => {
  const { delay, shouldGetError } = req.params;

  setTimeout(() => {
    if (shouldGetError === "true") {
      const error = new Error("You got an Error");
      return res.status(400).end(error.message);
    } else {
      return res.json([{ id: 1, name: "aaa" }, { id: 2, name: "bbb" }]);
    }
  }, delay * 1000);
});

module.exports = router;
