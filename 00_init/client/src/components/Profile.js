import React from "react";
import defaultUser from '../assets/defaultuser.jpg'

export default function Profile({ profile }) {
  return (
    <div key={profile.id}>
      <h3>{profile.name}</h3>
      <p>{profile.email}</p>
      <img src={profile.avatar || defaultUser} alt={profile.name} />
    </div>
  );
}
