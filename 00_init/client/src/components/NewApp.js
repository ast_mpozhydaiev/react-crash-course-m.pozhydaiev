import React, { Component } from "react";
import uuidv1 from "uuid/v1";
import Profile from "./Profile";

export default class NewApp extends Component {
  state = {
    data: null,
    loading: true
  };

  componentDidMount() {
    this.interval = setTimeout(() => {
      fetch("https://randomuser.me/api/?results=10")
        .then(res => res.json())
        .then(({results}) => {
          var mapped = results.map(item => ({
            id: uuidv1(),
            email: item.email,
            name: item.name.first,
            avatar: item.picture.large
          }));
          this.setState({ data: mapped, loading: false });
        })
        .catch();
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { data, loading } = this.state;
    return (
      <div>
        ANSWER
        <div>
          {loading && "loading..."}
          {!loading && data.map(p => <Profile key={p.id} profile={p} />)}
        </div>
      </div>
    );
  }
}
