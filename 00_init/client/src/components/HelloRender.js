import React from "react";

export default function HelloRender({ name, onIncrement, counter }) {
  return (
    <div>
      <span onClick={onIncrement}>Hello, {name}</span> - 
      counter {counter}
    </div>
  );
}
