import React from "react";
import "../sass/App.scss";
import Hello from "./Hello";

var arr = ["FirstName", "SecondName","3333","4444"];

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {arr.map(item => (
          <Hello key={item} name={item} />
        ))}
      </header>
    </div>
  );
}

export default App;
