import React, { Component } from "react";
import HelloRender from "./HelloRender";

export default class Hello extends Component {
  state = {
    counter: 0
  };

  componentDidMount(){
      console.log("Mount")
  }
  
  componentDidUpdate(){
    console.log("updated")
  }

  componentWillUnmount(){
      console.log("unmount")
  }

  onIncrement = () => {
    this.setState({ counter: this.state.counter + 1 });
  };

  render() {
    var { name } = this.props;
    return (
      <div>
        <HelloRender
          name={name}
          onIncrement={this.onIncrement}
          counter={this.state.counter}
        />
      </div>
    );
  }
}
